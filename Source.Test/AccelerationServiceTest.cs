﻿using Codenation.Challenge.Models;
using Codenation.Challenge.Services;
using System;
using System.Linq;
using Xunit;

namespace Codenation.Challenge
{
    public class AccelerationServiceTest
    {
        private readonly Acceleration _fakeAcceleration;

        public AccelerationServiceTest()
        {
            _fakeAcceleration = new Acceleration
            {
                Name = "name",
                Slug = "slug",
                CreatedAt = DateTime.Today
            };
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Should_Return_Right_Acceleration_When_Find_By_Id(int id)
        {
            var fakeContext = new FakeContext("AccelerationById");

            fakeContext.FillWith<Acceleration>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected = fakeContext.GetFakeData<Acceleration>().Find(x => x.Id == id);

                var service = new AccelerationService(context);
                var actual = service.FindById(id);

                Assert.Equal(expected, actual, new AccelerationIdComparer());
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(8)]
        [InlineData(9)]
        public void Should_Return_Right_Acceleration_List_When_Find_By_Company_Id(int companyId)
        {
            var fakeContext = new FakeContext("AccelerationListByCompanyId");

            fakeContext.FillWith<Acceleration>();
            fakeContext.FillWith<Candidate>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join acceleration in fakeContext.GetFakeData<Acceleration>() on candidate.AccelerationId equals acceleration.Id
                     where candidate.CompanyId == companyId
                     select acceleration).Distinct().ToList();

                var service = new AccelerationService(context);
                var actual = service.FindByCompanyId(companyId);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new AccelerationIdComparer());
            }
        }

        [Fact]
        public void Should_Add_Acceleration_When_Save()
        {
            var fakeContext = new FakeContext("SaveNewAcceleration");

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new AccelerationService(context);
                var actual = service.Save(_fakeAcceleration);

                Assert.NotEqual(0, actual.Id);
            }
        }

        [Fact]
        public void Should_Update_Acceleration_When_Save()
        {
            var fakeContext = new FakeContext("UpdateNewAcceleration");

            var expected = _fakeAcceleration;
            expected.Name = "update";

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new AccelerationService(context);
                var actual = service.Save(_fakeAcceleration);

                var id = actual.Id;
                actual.Name = "update";

                Assert.Equal(id, actual.Id);
                Assert.Equal(expected, actual, new AccelerationIdComparer());
            }
        }
    }
}
