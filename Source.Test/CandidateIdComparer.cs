﻿using Codenation.Challenge.Models;
using System.Collections.Generic;

namespace Codenation.Challenge
{
    internal class CandidateIdComparer : IEqualityComparer<Candidate>
    {
        public bool Equals(Candidate x, Candidate y)
        {
            return x.UserId == y.UserId && x.AccelerationId == y.AccelerationId && x.CompanyId == y.CompanyId;
        }

        public int GetHashCode(Candidate obj)
        {
            return obj.UserId.GetHashCode() + obj.AccelerationId.GetHashCode() + obj.CompanyId.GetHashCode();
        }
    }
}