﻿using Codenation.Challenge.Models;
using Codenation.Challenge.Services;
using System;
using System.Linq;
using Xunit;

namespace Codenation.Challenge
{
    public class CandidateServiceTest
    {
        private readonly Candidate _fakeCandidate;

        public CandidateServiceTest()
        {
            _fakeCandidate = new Candidate
            {
                Status = 1,
                CreatedAt = DateTime.Today,
                Acceleration = new Acceleration
                {
                    Name = "name",
                    Slug = "slug",
                    CreatedAt = DateTime.Today
                },
                Company = new Company
                {
                    Name = "name",
                    Slug = "slug",
                    CreatedAt = DateTime.Today
                },
                User = new User
                {
                    FullName = "full name",
                    Email = "email",
                    Nickname = "nickname",
                    Password = "pass",
                    CreatedAt = DateTime.Today
                }
            };
        }

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(2, 2, 2)]
        [InlineData(3, 3, 3)]
        public void Should_Return_Right_Candidate_When_Find_By_Id(int userId, int accelerationId, int companyId)
        {
            var fakeContext = new FakeContext("CandidateById");
            fakeContext.FillWith<Candidate>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected = (from candidate in fakeContext.GetFakeData<Candidate>()
                                where candidate.UserId == userId
                                    && candidate.AccelerationId == accelerationId
                                    && candidate.CompanyId == companyId
                                select candidate).First();

                var service = new CandidateService(context);
                var actual = service.FindById(userId, accelerationId, companyId);

                Assert.Equal(expected, actual, new CandidateIdComparer());
            }
        }

        [Theory]
        [InlineData(7)]
        [InlineData(8)]
        [InlineData(9)]
        public void Should_Return_Right_Candidate_List_When_Find_By_Company_Id(int id)
        {
            var fakeContext = new FakeContext("CandidateListByCompanyId");
            fakeContext.FillWith<Candidate>();
            fakeContext.FillWith<Company>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join company in fakeContext.GetFakeData<Company>() on candidate.CompanyId equals company.Id
                     where company.Id == id
                     select candidate).ToList();

                var service = new CandidateService(context);
                var actual = service.FindByCompanyId(id);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new CandidateIdComparer());
            }
        }

        [Fact]
        public void Should_Add_Candidate_When_Save()
        {
            var fakeContext = new FakeContext("SaveNewCandidate");

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new CandidateService(context);
                var actual = service.Save(_fakeCandidate);

                Assert.NotEqual(0, actual.UserId);
                Assert.NotEqual(0, actual.AccelerationId);
                Assert.NotEqual(0, actual.CompanyId);
            }
        }

        [Fact]
        public void Should_Update_Candidate_When_Save()
        {
            var fakeContext = new FakeContext("UpdateNewCandidate");

            var expected = _fakeCandidate;
            expected.Status = 2;

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new CandidateService(context);
                var actual = service.Save(_fakeCandidate);

                var userId = actual.UserId;
                var accelerationId = actual.AccelerationId;
                var companyId = actual.CompanyId;

                actual.Status = 2;

                Assert.Equal(expected.Status, actual.Status);

                Assert.Equal(userId, actual.UserId);
                Assert.Equal(accelerationId, actual.AccelerationId);
                Assert.Equal(companyId, actual.CompanyId);
            }
        }
    }
}
