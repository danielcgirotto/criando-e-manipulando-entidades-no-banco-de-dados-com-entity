﻿using Codenation.Challenge.Models;
using Codenation.Challenge.Services;
using System;
using System.Linq;
using Xunit;

namespace Codenation.Challenge
{
    public class ChallengeServiceTest
    {
        private readonly Models.Challenge _fakeChallenge;

        public ChallengeServiceTest()
        {
            _fakeChallenge = new Models.Challenge
            {
                Name = "name",
                Slug = "slug",
                CreatedAt = DateTime.Today
            };
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(2, 2)]
        [InlineData(4, 4)]
        public void Should_Return_Right_Challenge_When_Find_By_AccelerationId_And_UserId(int accelerationId, int userId)
        {
            var fakeContext = new FakeContext("ChallengeByAccelerationIdAndUserId");
            fakeContext.FillWith<Models.Challenge>();
            fakeContext.FillWith<Acceleration>();
            fakeContext.FillWith<User>();
            fakeContext.FillWith<Candidate>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected = (from candidate in fakeContext.GetFakeData<Candidate>()
                                join user in fakeContext.GetFakeData<User>() on candidate.UserId equals user.Id
                                join acceleration in fakeContext.GetFakeData<Acceleration>() on candidate.AccelerationId equals acceleration.Id
                                join challege in fakeContext.GetFakeData<Models.Challenge>() on acceleration.ChallengeId equals challege.Id
                                where candidate.AccelerationId == accelerationId && user.Id == userId
                                select challege).Distinct().ToList();

                var service = new ChallengeService(context);
                var actual = service.FindByAccelerationIdAndUserId(accelerationId, userId);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new ChallengeIdComparer());
            }
        }

        [Fact]
        public void Should_Add_Acceleration_When_Save()
        {
            var fakeContext = new FakeContext("SaveNewChallenge");

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new ChallengeService(context);
                var actual = service.Save(_fakeChallenge);

                Assert.NotEqual(0, actual.Id);
            }
        }

        [Fact]
        public void Should_Update_Acceleration_When_Save()
        {
            var fakeContext = new FakeContext("UpdateNewChallenge");

            var expected = _fakeChallenge;
            expected.Name = "update";

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new ChallengeService(context);
                var actual = service.Save(_fakeChallenge);

                var id = actual.Id;
                actual.Name = "update";

                Assert.Equal(id, actual.Id);
                Assert.Equal(expected.Name, actual.Name);
            }
        }
    }
}
