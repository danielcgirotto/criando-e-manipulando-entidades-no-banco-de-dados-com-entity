﻿using Codenation.Challenge.Models;
using Codenation.Challenge.Services;
using System;
using System.Linq;
using Xunit;

namespace Codenation.Challenge
{
    public class CompanyServiceTest
    {
        private readonly Company _fakeCompany;

        public CompanyServiceTest()
        {
            _fakeCompany = new Company
            {
                Name = "name",
                Slug = "slug",
                CreatedAt = DateTime.Today
            };
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Should_Return_Right_Company_When_Find_By_Id(int id)
        {
            var fakeContext = new FakeContext("CompanyById");
            fakeContext.FillWith<Company>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected = fakeContext.GetFakeData<Company>().Find(x => x.Id == id);

                var service = new CompanyService(context);
                var actual = service.FindById(id);

                Assert.Equal(expected, actual, new CompanyIdComparer());
            }
        }

        [Theory]
        [InlineData(7)]
        [InlineData(8)]
        [InlineData(9)]
        public void Should_Return_Right_Company_List_When_Find_By_AccelerationId(int accelerationId)
        {
            var fakeContext = new FakeContext("CompanyListByAccelerationId");
            fakeContext.FillWith<Candidate>();
            fakeContext.FillWith<Company>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join company in fakeContext.GetFakeData<Company>() on candidate.CompanyId equals company.Id
                     where candidate.AccelerationId == accelerationId
                     select company).ToList();

                var service = new CompanyService(context);
                var actual = service.FindByAccelerationId(accelerationId);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new CompanyIdComparer());
            }
        }

        [Theory]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(4)]
        public void Should_Return_Right_Company_List_When_Find_By_UserId(int userId)
        {
            var fakeContext = new FakeContext("CompanyListByUserId");
            fakeContext.FillWith<User>();
            fakeContext.FillWith<Candidate>();
            fakeContext.FillWith<Company>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join company in fakeContext.GetFakeData<Company>() on candidate.CompanyId equals company.Id
                     where candidate.UserId == userId
                     select company).Distinct().ToList();

                var service = new CompanyService(context);
                var actual = service.FindByUserId(userId);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new CompanyIdComparer());
            }
        }

        [Fact]
        public void Should_Add_Company_When_Save()
        {
            var fakeContext = new FakeContext("SaveNewCompany");

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new CompanyService(context);
                var actual = service.Save(_fakeCompany);

                Assert.NotEqual(0, actual.Id);
            }
        }

        [Fact]
        public void Should_Update_Company_When_Save()
        {
            var fakeContext = new FakeContext("UpdateNewCompany");

            var expected = _fakeCompany;
            expected.Name = "update";

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new CompanyService(context);
                var actual = service.Save(_fakeCompany);

                var id = actual.Id;
                actual.Name = "update";

                Assert.Equal(id, actual.Id);
                Assert.Equal(expected.Name, actual.Name);
            }
        }
    }
}
