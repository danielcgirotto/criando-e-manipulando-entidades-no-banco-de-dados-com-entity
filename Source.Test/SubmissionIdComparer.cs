using Codenation.Challenge.Models;
using System.Collections.Generic;

namespace Codenation.Challenge
{
    public class SubmissionIdComparer : IEqualityComparer<Submission>
    {
        public bool Equals(Submission x, Submission y)
        {
            return x.UserId == y.UserId && x.ChallengeId == y.ChallengeId;
        }

        public int GetHashCode(Submission obj)
        {
            return obj.User.GetHashCode() + obj.Challenge.GetHashCode();
        }
    }
}