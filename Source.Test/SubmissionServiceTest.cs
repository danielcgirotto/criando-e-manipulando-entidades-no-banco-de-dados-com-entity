﻿using Codenation.Challenge.Models;
using Codenation.Challenge.Services;
using System;
using System.Linq;
using Xunit;

namespace Codenation.Challenge
{
    public class SubmissionServiceTest
    {
        private readonly Submission _fakeSubmission;

        public SubmissionServiceTest()
        {
            _fakeSubmission = new Submission
            {
                Score = 100,
                CreatedAt = DateTime.Today,
                Challenge = new Models.Challenge
                {
                    Name = "name",
                    Slug = "slug",
                    CreatedAt = DateTime.Today
                },
                User = new User
                {
                    FullName = "full name",
                    Email = "email",
                    Nickname = "nickname",
                    Password = "pass",
                    CreatedAt = DateTime.Today
                }
            };
        }

        [Theory]
        [InlineData(2, 50.0)]
        [InlineData(3, 79.22)]
        public void Should_Return_Higher_Score_When_Find_By_ChallengeId(int challengeId, decimal score)
        {
            var fakeContext = new FakeContext("HigherScoreByChallengeId");
            fakeContext.FillWith<Submission>();
            fakeContext.FillWith<Models.Challenge>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected = (from submission in fakeContext.GetFakeData<Submission>()
                                join challenge in fakeContext.GetFakeData<Models.Challenge>() on submission.ChallengeId equals challenge.Id
                                where challenge.Id == challengeId
                                orderby submission.Score descending
                                select submission.Score).First();

                var service = new SubmissionService(context);
                var actual = service.FindHigherScoreByChallengeId(challengeId);

                Assert.Equal(expected, score);
                Assert.Equal(actual, score);

                Assert.Equal(expected, actual);
            }
        }

        [Theory]
        [InlineData(2, 1)]
        [InlineData(3, 2)]
        [InlineData(5, 3)]
        public void Should_Return_Right_Submission_List_When_Find_By_ChallengeId_And_AccelerationId(int challengeId, int accelerationId)
        {
            var fakeContext = new FakeContext("SubmissionListByChallengeIdAndAccelerationId");
            fakeContext.FillWith<Candidate>();
            fakeContext.FillWith<User>();
            fakeContext.FillWith<Submission>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join user in fakeContext.GetFakeData<User>() on candidate.UserId equals user.Id
                     join submission in fakeContext.GetFakeData<Submission>() on user.Id equals submission.UserId
                     where candidate.AccelerationId == accelerationId && submission.ChallengeId == challengeId
                     select submission).Distinct().ToList();

                var service = new SubmissionService(context);
                var actual = service.FindByChallengeIdAndAccelerationId(challengeId, accelerationId);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new SubmissionIdComparer());
            }
        }

        [Fact]
        public void Should_Add_Submission_When_Save()
        {
            var fakeContext = new FakeContext("SaveNewSubmission");

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new SubmissionService(context);
                var actual = service.Save(_fakeSubmission);

                Assert.NotEqual(0, actual.UserId);
                Assert.NotEqual(0, actual.ChallengeId);
            }
        }

        [Fact]
        public void Should_Update_Submission_When_Save()
        {
            var fakeContext = new FakeContext("UpdateNewSubmission");

            var expected = _fakeSubmission;
            expected.Score = 200;

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new SubmissionService(context);
                var actual = service.Save(_fakeSubmission);

                var userId = actual.UserId;
                var accelerationId = actual.ChallengeId;

                actual.Score = 200;

                Assert.Equal(expected.Score, actual.Score);
                Assert.Equal(expected, actual, new SubmissionIdComparer());
            }
        }
    }
}
