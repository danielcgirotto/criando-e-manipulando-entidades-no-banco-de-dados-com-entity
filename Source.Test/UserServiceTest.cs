using Codenation.Challenge.Models;
using Codenation.Challenge.Services;
using System;
using System.Linq;
using Xunit;

namespace Codenation.Challenge
{
    public class UserServiceTest
    {
        private readonly User _fakeUser;

        public UserServiceTest()
        {
            _fakeUser = new User
            {
                FullName = "full name",
                Email = "email",
                Nickname = "nickname",
                Password = "pass",
                CreatedAt = DateTime.Today
            };
        }

        [Theory]
        [InlineData("Velvet Grass")]
        [InlineData("Progesterone")]
        [InlineData("Temazepam")]
        [InlineData("Stool Softener")]
        public void Should_Return_Right_User_List_When_Find_By_Acceleration_Name(string name)
        {
            var fakeContext = new FakeContext("UserByCompanyId");

            fakeContext.FillWith<Candidate>();
            fakeContext.FillWith<Acceleration>();
            fakeContext.FillWith<User>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join acceleration in fakeContext.GetFakeData<Acceleration>() on candidate.AccelerationId equals acceleration.Id
                     join user in fakeContext.GetFakeData<User>() on candidate.UserId equals user.Id
                     where acceleration.Name == name
                     select user).Distinct().ToList();

                var service = new UserService(context);
                var actual = service.FindByAccelerationName(name);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new UserIdComparer());
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Should_Return_Right_User_List_When_Find_By_Company_Id(int companyId)
        {
            var fakeContext = new FakeContext("UserByCompanyId");

            fakeContext.FillWith<Candidate>();
            fakeContext.FillWith<User>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected =
                    (from candidate in fakeContext.GetFakeData<Candidate>()
                     join user in fakeContext.GetFakeData<User>() on candidate.UserId equals user.Id
                     where candidate.CompanyId == companyId
                     select user).Distinct().ToList();

                var service = new UserService(context);
                var actual = service.FindByCompanyId(companyId);

                Assert.Equal(expected.Count(), actual.Count());

                for (int i = 0; i < actual.Count(); i++)
                    Assert.Equal(expected[i], actual[i], new UserIdComparer());
            }
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Should_Return_Right_User_When_Find_By_Id(int id)
        {
            var fakeContext = new FakeContext("UserById");
            fakeContext.FillWith<User>();

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var expected = fakeContext.GetFakeData<User>().Find(x => x.Id == id);

                var service = new UserService(context);
                var actual = service.FindById(id);

                Assert.Equal(expected, actual, new UserIdComparer());
            }
        }

        [Fact]
        public void Should_Add_New_User_When_Save()
        {
            var fakeContext = new FakeContext("SaveNewUser");

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new UserService(context);
                var actual = service.Save(_fakeUser);

                Assert.NotEqual(0, actual.Id);
            }
        }

        [Fact]
        public void Should_Update_User_When_Save()
        {
            var fakeContext = new FakeContext("UpdateUser");

            var expected = _fakeUser;
            expected.FullName = "update";

            using (var context = new CodenationContext(fakeContext.FakeOptions))
            {
                var service = new UserService(context);
                var actual = service.Save(_fakeUser);

                var id = actual.Id;
                actual.FullName = "update";

                actual = service.Save(expected);

                Assert.Equal(expected, actual, new UserIdComparer());
            }
        }
    }
}
