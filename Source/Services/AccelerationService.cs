using Codenation.Challenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace Codenation.Challenge.Services
{
    public class AccelerationService : IAccelerationService
    {
        private readonly CodenationContext _context;

        public AccelerationService(CodenationContext context)
        {
            _context = context;
        }

        public IList<Acceleration> FindByCompanyId(int companyId)
        {
            return
                (from candidate in _context.Candidates
                 join acceleration in _context.Accelerations on candidate.AccelerationId equals acceleration.Id
                 where candidate.CompanyId == companyId
                 select acceleration).Distinct().ToList();
        }

        public Acceleration FindById(int id)
        {
            return _context.Accelerations.Where(x => x.Id == id).First();
        }

        public Acceleration Save(Acceleration acceleration)
        {
            if (acceleration.Id == 0)
            {
                _context.Add(acceleration);
                _context.SaveChanges();

                return acceleration;
            }

            _context.Update(acceleration);

            return acceleration;
        }
    }
}
