using System.Collections.Generic;
using System.Linq;
using Codenation.Challenge.Models;

namespace Codenation.Challenge.Services
{
    public class CandidateService : ICandidateService
    {
        private readonly CodenationContext _context;

        public CandidateService(CodenationContext context)
        {
            _context = context;
        }

        public IList<Candidate> FindByAccelerationId(int accelerationId)
        {
            return
                (from acceleration in _context.Accelerations
                 join candidate in _context.Candidates on acceleration.Id equals candidate.AccelerationId
                 where acceleration.Id == accelerationId
                 select candidate).ToList();
        }

        public IList<Candidate> FindByCompanyId(int companyId)
        {
            return
                (from candidate in _context.Candidates
                 join company in _context.Companies on candidate.CompanyId equals company.Id
                 where company.Id == companyId
                 select candidate).ToList();
        }

        public Candidate FindById(int userId, int accelerationId, int companyId)
        {
            return
                (from candidate in _context.Candidates
                 where candidate.UserId == userId
                 where candidate.AccelerationId == accelerationId
                 where candidate.CompanyId == companyId
                 select candidate).First();
        }

        public Candidate Save(Candidate candidate)
        {
            if (candidate.UserId == 0 && candidate.AccelerationId == 0 && candidate.CompanyId == 0)
            {
                _context.Add(candidate);
                _context.SaveChanges();

                return candidate;
            }

            _context.Update(candidate);

            return candidate;
        }
    }
}
