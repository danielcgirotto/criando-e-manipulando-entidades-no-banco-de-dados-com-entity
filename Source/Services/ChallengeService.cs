using Codenation.Challenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace Codenation.Challenge.Services
{
    public class ChallengeService : IChallengeService
    {
        private readonly CodenationContext _context;

        public ChallengeService(CodenationContext context)
        {
            _context = context;
        }

        public IList<Models.Challenge> FindByAccelerationIdAndUserId(int accelerationId, int userId)
        {
            return
                (from candidate in _context.Candidates
                 join acceleration in _context.Accelerations on candidate.AccelerationId equals acceleration.Id
                 join user in _context.Users on candidate.UserId equals user.Id
                 join challenge in _context.Challenges on acceleration.ChallengeId equals challenge.Id
                 where acceleration.Id == accelerationId && user.Id == userId
                 select challenge).Distinct().ToList();
        }

        public Models.Challenge Save(Models.Challenge challenge)
        {
            if (challenge.Id == 0)
            {
                _context.Add(challenge);
                _context.SaveChanges();

                return challenge;
            }

            _context.Update(challenge);

            return challenge;
        }
    }
}