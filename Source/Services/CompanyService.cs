using Codenation.Challenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace Codenation.Challenge.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly CodenationContext _context;

        public CompanyService(CodenationContext context)
        {
            _context = context;
        }

        public IList<Company> FindByAccelerationId(int accelerationId)
        {
            return
                (from candidate in _context.Candidates
                 join company in _context.Companies on candidate.CompanyId equals company.Id
                 where candidate.AccelerationId == accelerationId
                 select company).ToList();
        }

        public Company FindById(int id)
        {
            return _context.Companies.Where(x => x.Id == id).First();
        }

        public IList<Company> FindByUserId(int userId)
        {
            return
                (from candidate in _context.Candidates
                 join company in _context.Companies on candidate.CompanyId equals company.Id
                 where candidate.UserId == userId
                 select company).Distinct().ToList();
        }

        public Company Save(Company company)
        {
            if (company.Id == 0)
            {
                _context.Add(company);
                _context.SaveChanges();

                return company;
            }

            _context.Update(company);

            return company;
        }
    }
}