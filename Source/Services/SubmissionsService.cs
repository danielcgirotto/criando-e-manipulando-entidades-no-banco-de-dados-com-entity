using Codenation.Challenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace Codenation.Challenge.Services
{
    public class SubmissionService : ISubmissionService
    {
        private readonly CodenationContext _context;

        public SubmissionService(CodenationContext context)
        {
            _context = context;
        }

        public IList<Submission> FindByChallengeIdAndAccelerationId(int challengeId, int accelerationId)
        {
            return
                (from candidate in _context.Candidates
                 join user in _context.Users on candidate.UserId equals user.Id
                 join submission in _context.Submissions on user.Id equals submission.UserId
                 where candidate.AccelerationId == accelerationId && submission.ChallengeId == challengeId
                 select submission).Distinct().ToList();
        }

        public decimal FindHigherScoreByChallengeId(int challengeId)
        {
            return
                (from submission in _context.Submissions
                 join challenge in _context.Challenges on submission.ChallengeId equals challenge.Id
                 where challenge.Id == challengeId
                 orderby submission.Score descending
                 select submission.Score).First();
        }

        public Submission Save(Submission submission)
        {
            if (submission.UserId == 0 && submission.ChallengeId == 0)
            {
                _context.Add(submission);
                _context.SaveChanges();

                return submission;
            }

            _context.Update(submission);

            return submission;
        }
    }
}
