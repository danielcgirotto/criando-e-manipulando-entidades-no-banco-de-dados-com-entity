using Codenation.Challenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace Codenation.Challenge.Services
{
    public class UserService : IUserService
    {
        private readonly CodenationContext _context;

        public UserService(CodenationContext context)
        {
            _context = context;
        }

        public IList<User> FindByAccelerationName(string name)
        {
            return
                (from candidate in _context.Candidates
                 join acceleration in _context.Accelerations on candidate.AccelerationId equals acceleration.Id
                 join user in _context.Users on candidate.UserId equals user.Id
                 where acceleration.Name == name
                 select user).Distinct().ToList();
        }

        public IList<User> FindByCompanyId(int companyId)
        {
            return
                (from candidate in _context.Candidates
                 join user in _context.Users on candidate.UserId equals user.Id
                 where candidate.CompanyId == companyId
                 select user).Distinct().ToList();
        }

        public User FindById(int id)
        {
            return (from user in _context.Users
                    where user.Id == id
                    select user).FirstOrDefault();
        }

        public User Save(User user)
        {
            if (user.Id == 0)
            {
                _context.Users.Add(user);
                _context.SaveChanges();

                return user;
            }

            _context.Update(user);

            return user;
        }
    }
}
